from django.contrib import admin

from .models import Person

@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    fields =("name", "telegram")
    list_display =("id", "name", "telegram")
    list_display_links =("id", "name", "telegram")

from app.internal.admin.admin_user import AdminUserAdmin

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"
